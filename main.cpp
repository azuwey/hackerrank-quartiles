/**
 * Author: Zarandi David (Azuwey)
 * Date: 03/06/2019
 * Source: HackerRank - https://www.hackerrank.com/challenges/s10-quartiles/problem
 **/
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <vector>
using namespace std;

int median(vector<int> arr) {
  int n = arr.size();
  int m = arr[n / 2];
  if (n % 2 == 0) {
    m = (m + arr[n / 2 - 1]) / 2;
  }
  return m;
}

int main() {
  int n;
  cin >> n;

  vector<int> arr = vector<int>(n);
  for (int i = 0; i < n; ++i) {
    cin >> arr[i];
  }

  sort(arr.begin(), arr.end());

  vector<int> lower;
  vector<int> upper;

  for (size_t i = 0; i < (n / 2); i++) {
    lower.push_back(arr[i]);
  }

  if (n % 2 == 0) {
    for (size_t i = (n / 2); i < n; i++) {
      upper.push_back(arr[i]);
    }
  } else {
    for (size_t i = (n / 2) + 1; i < n; i++) {
      upper.push_back(arr[i]);
    }
  }

  cout << median(lower) << endl
       << median(arr) << endl
       << median(upper);

  return 0;
}
